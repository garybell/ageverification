<?php
/**
 * Extension of data subject to allow for drivers license and state
 *
 * (c) 2020 Gary Bell <gary@garybell.co.uk>
 *
 * @package AgeVerification
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace GaryBell\AgeVerification;


final class AgeCheckedDataSubject extends GenericDataSubject
{
    private string $state = '';
    private string $driversLicense = '';

    public function __construct(
        string $firstName = '',
        string $surname = '',
        string $building = '',
        string $street = '',
        string $postalCode = '',
        string $country = '',
        string $email = '',
        string $dateOfBirth = '',
        string $state = '',
        string $driversLicense = ''
    ) {
        parent::__construct($firstName, $surname, $building, $street, $postalCode, $country, $email, $dateOfBirth);

        $this->state = $state;
        $this->driversLicense = $driversLicense;
    }

    /**
     * @return string
     */
    public function getState(): string
    {
        return $this->state;
    }

    /**
     * @param string $state
     */
    public function setState(string $state): void
    {
        $this->state = $state;
    }

    /**
     * @return string
     */
    public function getDriversLicense(): string
    {
        return $this->driversLicense;
    }

    /**
     * @param string $driversLicense
     */
    public function setDriversLicense(string $driversLicense): void
    {
        $this->driversLicense = $driversLicense;
    }


}
